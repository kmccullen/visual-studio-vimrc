# Visual Studio .vimrc

* Install [VsVim](https://marketplace.visualstudio.com/items?itemName=JaredParMSFT.VsVim)
* Download .vimrc to `C:\Users\%USER%\.vimrc`
